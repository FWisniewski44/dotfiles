#!/usr/bin/env bash

# session
lxsession &
udiskie &

# reloading the wallpaper
nitrogen --restore &

# starting the xscreensaver
xscreensaver --no-splash

# launch nm-applet and volumeicon
nm-applet &
volumeicon &

# Launch notification daemon
dunst \
-geom "280x50-10+38" -frame_width "1" -font "IBM Plex Sans 10" \
-lb "#0F131FFF" -lf "#82dbf4FF" -lfr "#548FABFF" \
-nb "#0F131FFF" -nf "#82dbf4FF" -nfr "#548FABFF" \
-cb "#2E3440FF" -cf "#BF616AFF" -cfr "#BF616AFF" &

# power manager and picom start
#xfce4-power-manager &
picom &
