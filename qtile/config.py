# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

###############################################################################
################################### QTILE CONFIG ##############################
###############################################################################

# IMPORT section: parts of this were copied over from DTs config.py

from typing import List  # noqa: F401

import os
import re
import socket
import subprocess

from libqtile import qtile, bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.command import lazy
from libqtile.utils import guess_terminal

# misc
home_dir = os.path.expanduser("~")
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

# j4 and dmenu
dmenu_conf = "-c -i -l 10 -nb '#0F131F' -nf '#82dbf4' -sb '#5b99aa' -sf '#82dbf4' -fn 'IBM Plex Sans:size=12'"
#j4 = f"j4-dmenu-desktop --no-generic --term='{terminal}' --dmenu=\"dmenu -p 'Run App:' {dmenu_conf}\""

# font vars
myFont1 = "IBM Plex Sans Regular"
myFont2 = "JetBrains Mono Regular"

# often used keys
mod = "mod4"

# often used apps
terminal = f"alacritty --config-file {home_dir}/.config/alacritty/alacritty.yml"
browser = "brave "
wallpaper = "nitrogen"
filemanager = "pcmanfm"
graphical_editor="atom"

keys = [
    # Switch the focus between windows
    Key([mod], "h", lazy.layout.left(),
        desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(),
        desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(),
        desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(),
        desc="Move focus up"),
    Key([mod, "shift"], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(),
        desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(),
        desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(),
        desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes

    # keybindings for some applications
    Key([mod], "Return", lazy.spawn(terminal),
        desc="Launch terminal"),
    Key([mod], "b", lazy.spawn(browser),
        desc="Launch browser"),
    Key([mod], "n", lazy.spawn(wallpaper),
        desc="Launch nitrogen"),
    Key([mod, "shift"], "Return", lazy.spawn(filemanager),
        desc="Launch dolphin"),
    Key([mod], "a", lazy.spawn(graphical_editor),
        desc="Launch a graphical text editor"),
    #Key([mod, "mod1"], "l", lazy.spawn("xscreensaver-command -lock"),
    #    desc="Lock screen with xscreensaver"),
    Key([mod], "e", lazy.spawn("emacs"),
        desc="Launch Doom Emacs"),
    Key([mod], "w", lazy.spawn(terminal + " --hold -e curl wttr.in/Bamberg &"),
        desc="Get weather for Bamberg"),
    Key([mod, "control"], "3", lazy.spawn("mate-screenshot"),
        desc="Open mate-screenshot tool"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(),
        desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(),
        desc="Kill focused window"),

    # qtile specific commands
    Key([mod, "control"], "r", lazy.restart(),
        desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(),
        desc="Shutdown Qtile"),
    Key([mod], "space", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
    Key([mod, "control"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    # audio and brightness
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 10%+"),
        desc="Raise the volume via amixer"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 10%-"),
        desc="Lower the volume via amixer"),
    Key([], "XF86AudioMute", lazy.spawn("amixer -D pulse set Master 1+ toggle"),
        desc="Mute audio via amixer"),

    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s 100+"),
        desc="Screen brightness up"),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 100-"),
        desc="Screen brightness down"),

    # useful scripts
    Key([mod, "control"], "p", lazy.spawn("powerspec-qtile"),
        desc="OTB's shutdown/logout/reboot script, modified with logout option"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    #layout.Columns(border_focus_stack=['#d75f5f', '#8f3d3d'], margin=4, border_width=2),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    #layout.Bsp(border_width=2, margin=4),
    # layout.Matrix(),
    layout.MonadTall(border_width=3, margin=10,
        font=myFont1, font_size=10,
        border_focus="#064273", border_normal="#0F131F"),
    #layout.MonadWide(border_width=2, margin=4),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    #layout.Floating(border_focus="#064273", border_normal="#0F131F", border_width=3, font=myFont1),
    layout.Max()
]

# defining a list of tuples with colour hexcodes
colours = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"], # window name
          ["#ecbbfb", "#ecbbfb"], # background for inactive screens
          ["#BDE6FB", "#BDE6FB"], # zorinOS dark mode, accent color
          ["#88A5B4", "#88A5B4"]  # zorinOS dark mode, accent color, a bit darker
          ]



widget_defaults = dict(
    font='IBM Plex Sans',
    fontsize=12,
    padding=3,
    background="#282c34"
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(linewidth = 0, padding = 6,
                    background=colours[0], foreground=colours[0]),
                widget.Image(filename="/usr/share/endeavouros/endeavouros-icon.png",
                    background = colours[0],
                    margin = 3,
                    scale=True,
                    #mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn("rofi -show run")}
                             ),
                widget.Sep(linewidth = 0, padding = 6,
                    background=colours[0], foreground=colours[0]),
                widget.CurrentLayoutIcon(custom_icon_paths=os.path.expanduser("~/.config/qtile/CurrentLayoutIcons"), scale=0.7),
                widget.CurrentLayout(foreground=colours[2]),
                widget.Sep(linewidth = 0, padding = 6,
                    background=colours[0], foreground=colours[0]),
                widget.GroupBox(active=colours[2], inactive=colours[-2]),
                widget.Sep(linewidth = 0, padding = 6,
                    background=colours[0], foreground=colours[0]),
                widget.Prompt(prompt=prompt, font=myFont1, padding=10, foreground=colours[-2]),
                widget.WindowName(max_chars=40, padding=10, foreground=colours[-2]),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.OpenWeather(app_key='c42d6a44fc9b414221082d626c73362c', format='{location_city}: {main_temp} °{units_temperature} | {weather}',
                    location="Bamberg", language="en", font=myFont1, foreground=colours[2], padding=5),
                widget.Memory(foreground=colours[-2], padding=5),
                widget.Volume(padding=5, foreground=colours[2]),
                widget.Battery(battery=1, format="{percent:2.0%}", foreground=colours[-2], padding=5),
                widget.CheckUpdates(distro='Arch_checkupdates', padding=5,
                    colour_have_updates="#ff0000", colour_no_updates=colours[-2],
                    #mouse_callbacks={'Button1': lambda : qtile.cmd_spawn(terminal+" -e sudo pacman -Syu")}
                                    ),
                widget.Clock(format='%d-%m-%Y | %a, %H:%M', foreground=colours[2]),
                widget.Systray(foreground=colours[-2]),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = False
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

## autostart.sh
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
