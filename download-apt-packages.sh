#!/usr/bin/env sh

xfce4-terminal --hold -e "sudo apt update && sudo apt upgrade && sudo apt install nala fonts-ubuntu fonts-ubuntu-console fonts-ubuntu-title firefox-esr xrandr arandr chromium emacs-gtk vim neofetch texlive-full gufw htop kitty l3afpad libreoffice-gnome libreoffice-gtk3 lxsession lxpolkit meld xfce4 xfce4-goodies bspwm sxhkd picom polybar zathura zim zim-tools"
