#+title: Emacs Configs
#+author: Florian Wisniewski

* config.el
** Font configuration

(setq doom-font (font-spec :family "mononoki" :size 14)
      doom-variable-pitch-font (font-spec :family "mononoki" :size 12))

** Theme

(setq doom-theme 'doom-sourcerer)

** Latex configuration

;; latex class
(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("org-plain-latex"
               "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; latex compiler with xelatex
;; http://orgmode.org/worg/org-faq.html#using-xelatex-for-pdf-export
;; latexmk runs pdflatex/xelatex (whatever is specified) multiple times
;; automatically to resolve the cross-references.
(setq org-latex-pdf-process '("latexmk -xelatex -quiet -shell-escape -f %f"))

;; set org latex compiler engine
;;(setq org-latex-compiler "xelatex") ; introduced in org 9.0

* init.el

Activate:
+ ivy
+ doom-quit
+ zen
+ latex
+ markdown
+ nix
+ (org +pretty)
+ python
+ sh
+ web
+ yaml
