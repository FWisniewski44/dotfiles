#
# ~/.bashrc
#

#Ibus settings if you need them
#type ibus-setup in terminal to change settings and start the daemon
#delete the hashtags of the next lines and restart
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=dbus
#export QT_IM_MODULE=ibus

PS1='\[\e[0m\]\u\[\e[0m\]@\[\e[0m\]\h\[\e[0m\]: \[\e[0m\]\w \[\e[0m\]\$ \[\e[0m\]'

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

#trying to figure out a good pandoc alias
alias eisvogel='pandoc --from markdown --template eisvogel --listings --pdf-engine=xelatex'

#misc
alias youtube-dl-mp3='youtube-dl --playlist-start 1 --playlist-end 12 -x --audio-format "mp3" --audio-quality 0 -v'

#list
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='exa -la'
alias l='ls'
alias l.="ls -A | egrep '^\.'"
alias ..='cd ..'

#fix obvious typos
alias cd..='cd ..'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#readable output
alias df='df -h'

#continue download
alias wget="wget -c"

#userlist
alias userlist="cut -d: -f1 /etc/passwd"

# Aliases for software managment
# pacman or pm
#alias update='sudo pacman -Syu'
#alias install='sudo pacman -S'
#alias search='sudo pacman -Ss'
#alias remove='sudo pacman -R'

# aura: aliases for AUR
#alias aur-update='sudo aura -Ayu'
#alias aur-install='sudo aura -A'
#alias aur-search='sudo aura -As'
#alias aur-remove='sudo aura -R'
#alias orphans='sudo aura -O'

# for void linux
# alias update='sudo xbps-install -Su'
# alias sync='sudo xbps-install -S'
# alias install='sudo xbps-install'
# alias search='sudo xbps-query -Rs'
# alias remove='sudo xbps-remove'
# alias orphans='sudo xbps-remove -o'

# for fedora/RHEL based distros
#alias update='sudo dnf update'
#alias upgrade='sudo dnf upgrade'
#alias search='sudo dnf search'
#alias install='sudo dnf install'
#alias remove='sudo dnf remove'
#alias info='sudo dnf info'

# for debian based distros --- this depends on nala, an apt replacement that actually makes apt output readable
alias update='sudo nala update'
alias upgrade='sudo nala upgrade'
alias upgradable='sudo nala list --upgradable'
alias autoremove='sudo nala autoremove'
alias install='sudo nala install'
alias search='sudo nala search'
alias remove='sudo nala remove'

#add new fonts
alias update-fc='sudo fc-cache -fv'

#gpg
#verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
alias fix-gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias fix-key="[ -d ~/.gnupg ] || mkdir ~/.gnupg ; cp /etc/pacman.d/gnupg/gpg.conf ~/.gnupg/ ; echo 'done'"

#shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="sudo reboot"

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# reporting tools - install when not installed
# install neofetch
neofetch
# install screenfetch
#screenfetch
# install ufetch-git
#ufetch
# install ufetch-arco-git
#ufetch-arco
# install arcolinux-paleofetch-git
#paleofetch
# install alsi
#alsi
# install arcolinux-bin-git - standard on ArcoLinux isos (or sfetch - smaller)
#hfetch
# install lolcat
# neofetch | lolcat

PATH=$PATH:~/.local/bin

export PATH=$HOME/.emacs.d/bin:$PATH
