"my .vimrc config file - Florian Wisniewski - started with vim on 06-05-2021

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.

" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
Plugin 'sheerun/vim-polyglot'

" plugins DT recommended
Plugin 'vifm/vifm.vim'
Plugin 'vim-python/python-syntax'
Plugin 'kovetskiy/sxhkd-vim'
Plugin 'frazrepo/vim-rainbow'

" plugins: NERDtree & associated
Plugin 'preservim/nerdtree'
" add icons and syntax highlighting to NERDtree
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'

" aesthetics
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'junegunn/goyo.vim'
"Plugin 'itchyny/lightline.vim'

" Plugins for markdown writing in vim:
Plugin 'tpope/vim-markdown', {'name': 'pope-vim-markdown'}
Plugin 'plasticboy/vim-markdown'

" colorschemes
Plugin 'morhetz/gruvbox'
Plugin 'jacoborus/tender.vim'
Plugin 'joshdick/onedark.vim'
Plugin 'glepnir/oceanic-material'
Plugin 'haishanh/night-owl.vim'
Plugin 'ghifarit53/tokyonight-vim'
Plugin 'liuchengxu/space-vim-dark'

" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General Settings for VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" general stuff
set number relativenumber					" display line numbers
set t_Co=256 					" something about displaying colors (from DTs .vimrc)
set clipboard=unnamedplus 			" copying/pasting between vim and other programs (also from DTs .vimrc)
syntax on 					" Turning syntax on
set wildmenu 					" wildmenu for tab completion anywhere
set mouse=nicr
set splitright
set splitbelow

" writing and more
set shiftwidth=4 				" one tab == four spaces (see DTs .vimrc)
set tabstop=4 					" -"- (also DTs .vimrc)

" NERDtree autostart (comment or uncomment, as we wish)
"autocmd vimenter * NERDTree

" some settings for .md writing
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_math = 1



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" setting the colorscheme
syntax enable
colorscheme tokyonight
"let g:tokyonight_style = 'storm'   "available == night, storm
"let g:tokyonight_enable_italic = 1
let g:airline_theme='simple'
let g:solarized_termcolors=256

