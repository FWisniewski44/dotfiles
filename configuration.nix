# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";

  boot.loader = {
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot/efi";
    };
    grub = {
      enable = true;
      device = "nodev";
      efiSupport = true;
      theme = pkgs.nixos-grub2-theme;
      configurationLimit = 5;
    };
  };

  networking.hostName = "nixos"; # Define your hostname.
 # networking.wireless = {
 #   enable = true; # enable networking (wireless) via wpa_supplicant
 #   networks = {
 #     eduroam = {
 #       auth = ''
 #       proto=RSN
 #       key_mgmt=WPA-EAP
 #       eap=PEAP
 #       identity="ba066916@uni-bamberg.de"
 #       password=neTe!e3p
 #       #domain_suffix_match="uni-bamberg.de"
 #       domain_suffix_match="uni-bamberg.de"
 #       anonymous_identity="eduroam@uni-bamberg.de"
 #       phase1="peaplabel=0"
 #       phase2="auth=MSCHAPV2"
 #      #ca_cert="/etc/ssl/certs/ca-bundle.crt"
 #       ca_cert="/etc/ssl/certs/ca-bundle.crt"
 #       '';
 #     };
 #   };
 # };


  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;
  # networking.networkmanager.unmanaged = ["eduroam"];

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.utf8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.utf8";
    LC_IDENTIFICATION = "de_DE.utf8";
    LC_MEASUREMENT = "de_DE.utf8";
    LC_MONETARY = "de_DE.utf8";
    LC_NAME = "de_DE.utf8";
    LC_NUMERIC = "de_DE.utf8";
    LC_PAPER = "de_DE.utf8";
    LC_TELEPHONE = "de_DE.utf8";
    LC_TIME = "de_DE.utf8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment. (including GNOME flashback version with Metacity WM)
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # # Alternatively, use KDE Plasma.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "de";
    xkbVariant = "nodeadkeys";
  };

  # Configure console keymap
  console.keyMap = "de-latin1-nodeadkeys";

  # # Configuration for auto-updates, if wanted
  # system.autoUpgrade.enable = true;
  # # allowReboot will reboot the system once there are any important updates that would need this (e. g. Kernel)
  # system.autoUpgrade.allowReboot = true;


  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = [pkgs.hplipWithPlugin];
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.fw = {
    isNormalUser = true;
    description = "fw";
    extraGroups = [ "networkmanager" "wheel" ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Making fonts available
  fonts.fonts = with pkgs; [
    fira
    fira-code
    gentium
    roboto-slab
    ubuntu_font_family
    zilla-slab
]; 

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    ## gnome shell extensions
    gnomeExtensions.espresso
    gnomeExtensions.blur-my-shell
    # gnomeExtensions.openweather

    ## utilities
    bash-completion
    fish
    zsh
    cacert
    gnupg
    exa
    syncthing # no gui --- broken as of june 2022
    ripgrep
    findutils
    fd
    system-config-printer
    sudo
    neofetch
    freshfetch
    networkmanager
    sstp
    networkmanager-sstp
    networkmanagerapplet
    ffmpeg
    # bspwm
    # polybar
    picom
    rofi-wayland
    rofi-vpn
    rofi-rbw
    hplipWithPlugin
    mupdf
    openvpn
    networkmanager-openvpn
    gnome.networkmanager-openvpn

    ## general apps
    vim
    git
    wget
    curl
    # alacritty
    # kitty
    gparted
    htop
    spectacle
    pavucontrol
    okular
    firefox
    brave
    emacs
    leafpad
    thunderbird
    gimp
    inkscape
    pandoc
    texlive.combined.scheme-full # full has 4000 packages, medium has essential stuff (around 1100)
    # biber # is not needed if texlive.scheme-full is installed, as it is already included there
    # xsecurelock
    # nitrogen
    onlyoffice-bin
    netlogo
    vscode-fhs

    ## fonts
    mononoki
    inter
    ibm-plex
    jetbrains-mono
    open-sans
    gentium
    roboto
    roboto-slab
    roboto-mono
    fira
    fira-code
    ubuntu_font_family
    zilla-slab

    ## icon schemes
    kora-icon-theme # if plasma is used, use this icon scheme
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leavecatenate(variables, "bootdev", bootdev)
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
